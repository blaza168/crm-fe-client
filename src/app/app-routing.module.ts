import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomepageComponent} from './components/homepage/homepage.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {EstablishmentSubpageComponent} from './components/establishment-subpage/establishment-subpage.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomepageComponent,
  },
  {
    path: 'podstranka/:establishmentId/:subpageRoute',
    component: EstablishmentSubpageComponent,
  },
  {
    path: 'nenalezeno',
    component: NotFoundComponent,
  },
  {
    path: '**',
    redirectTo: 'nenalezeno',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

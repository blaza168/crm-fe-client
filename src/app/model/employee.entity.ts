import {EstablishmentEntity} from './establishment.entity';

export class EmployeeEntity {
  public id: number;
  public firstName: string;
  public lastName: string;
  public route: string;
  public phone: string;
  public establishments: EstablishmentEntity[];
}

export class EstablishmentSubpageEntity {
  id: number;
  name: string;
  route: string;
  content: string;
  galleryFiles: GalleryFile[];
}

export class GalleryFile {
  id: number;
  description: string;
}

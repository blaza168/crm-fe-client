import {EmployeeEntity} from './employee.entity';
import {EstablishmentSubpageEntity} from './establishment-subpage.entity';

export class EstablishmentEntity {
  public id?: number;
  public name: string;
  public route: string;
  public employees?: EmployeeEntity[];
  public subpages: EstablishmentSubpageEntity[];
}

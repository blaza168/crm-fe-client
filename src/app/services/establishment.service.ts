import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {EstablishmentEntity} from '../model/establishment.entity';

@Injectable()
export class EstablishmentService {

  private static readonly ESTABLISHMENT_URL = '/api/crm/establishment';

  constructor(private http: HttpClient) {}

  public getEstablishment(route: string): Observable<EstablishmentEntity> {
    return this.http.get<EstablishmentEntity>(EstablishmentService.ESTABLISHMENT_URL + '/' + route);
  }

  public listEstablishments(): Observable<EstablishmentEntity[]> {
    return this.http.get<EstablishmentEntity[]>(EstablishmentService.ESTABLISHMENT_URL + '/list');
  }
}

import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';

export class Hash {
  category: string;
  content: KVPair[] = [];

  public setValue(key: string, value: string): void {
    for (const kvPair of this.content) {
      if (kvPair.key === key) {
        kvPair.value = value;
      }
    }
  }

  public getValue(key: string): string {
    for (const kvPair of this.content) {
      if (kvPair.key === key) {
        return kvPair.value;
      }
    }

    return undefined;
  }
}

export class KVPair {
  key: string;
  value: string;
}

@Injectable()
export class KvService {

  constructor(private http: HttpClient) {}


  public storeHash(category: string, content: {}): Observable<any> {
    return this.http.post('/api/crm/kv', this.toHash(category, content));
  }

  public getHash(category: string): Observable<Hash> {
    return this.http.get<Hash>('/api/crm/kv/' + category).pipe(map((hash: Hash) => {
      const h = new Hash();
      h.category = hash.category;
      for (const pair of hash.content) {
        h.content.push(pair);
      }

      return h;
    }));
  }

  private toHash(category: string, content: {}): Hash {
    const hash = new Hash();

    hash.category = category;

    hash.content = [];
    for (const key in content) {
      if (content.hasOwnProperty(key)) {
        const pair = new KVPair();
        pair.key = key;
        pair.value = content[key];
        hash.content.push(pair);
      }
    }

    return hash;
  }
}

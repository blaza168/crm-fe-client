import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class FileService {

  private static readonly CORE_URL = '/api/crm/image/';

  constructor(private http: HttpClient) {}


  public listFiles(category: string, key: string): Observable<number[]> {
    return this.http.get<number[]>(FileService.CORE_URL + `${category}/${key}`);
  }
}

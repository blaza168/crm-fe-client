import {Observable} from 'rxjs';
import {EmployeeEntity} from '../model/employee.entity';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {EstablishmentEntity} from '../model/establishment.entity';


@Injectable()
export class EmployeeService {

  constructor(private http: HttpClient) {}

  public listEmployees(): Observable<EmployeeEntity[]> {
    return this.http.get<EmployeeEntity[]>('/api/crm/employees');
  }

  public getEmployee(route: string): Observable<EmployeeEntity> {
    return this.http.get<EmployeeEntity>('/api/crm/employee/' + route);
  }

  public assignToEstablishment(employeeId: number, establishments: EstablishmentEntity[]): Observable<void> {
    const payload = {
      employeeId: employeeId,
      establishmentIds: establishments.map((e) => e.id),
    };

    return this.http.post<void>('/api/crm/employee/assign', payload);
  }

  public updateEmployee(vals: {}): Observable<any> {
    return this.http.put('/api/crm/employee', vals);
  }

  public createEmployee(vals: {}): Observable<any> {
    return this.http.post('/api/crm/employee', vals);
  }


  // private toEmployee(vals: {}): EmployeeEntity {
  //   const employee = new EmployeeEntity();
  //
  //   employee.firstName = vals['firstName'];
  //   employee.lastName = vals['lastName'];
  //
  //   if (vals['id']) {
  //     employee.id = vals['id'];
  //   }
  //
  //   return employee;
  // }
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EstablishmentSubpageEntity} from '../model/establishment-subpage.entity';

@Injectable()
export class EstablishmentSubpageService {

  constructor(private http: HttpClient) {}

  public getSubpage(establishmentId: number, subpageRoute: string): Observable<EstablishmentSubpageEntity> {
    return this.http.get<EstablishmentSubpageEntity>(`/api/crm/establishment-subpage/${establishmentId}/${subpageRoute}`);
  }

}

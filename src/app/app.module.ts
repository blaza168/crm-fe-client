import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomepageComponent } from './components/homepage/homepage.component';
import {HttpClientModule} from '@angular/common/http';
import {KvService} from './services/kv.service';
import { NavigationComponent } from './components/navigation/navigation.component';
import {NgxNavbarModule} from 'ngx-bootstrap-navbar';
import {EstablishmentService} from './services/establishment.service';
import {LoadingComponent} from './components/shared/loading/loading.component';
import {MatButtonModule, MatDialogModule, MatListModule, MatProgressSpinnerModule, MatTooltipModule} from '@angular/material';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FooterComponent } from './components/footer/footer.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FileService} from './services/file.service';
import { EstablishmentSubpageComponent } from './components/establishment-subpage/establishment-subpage.component';
import {EstablishmentSubpageService} from './services/establishment-subpage.service';
import { ImageGalleryComponent } from './components/shared/images/image-gallery/image-gallery.component';
import { ImageDialogComponent } from './components/shared/images/image-dialog/image-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    NavigationComponent,
    LoadingComponent,
    NotFoundComponent,
    FooterComponent,
    EstablishmentSubpageComponent,
    ImageGalleryComponent,
    ImageDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,

    // design
    NgbModule,
    NgxNavbarModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatTooltipModule,
    NgbModule,
    MatDialogModule,
    MatButtonModule,
  ],
  providers: [
    KvService,
    EstablishmentService,
    FileService,
    EstablishmentSubpageService,
  ],
  entryComponents: [
    ImageDialogComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

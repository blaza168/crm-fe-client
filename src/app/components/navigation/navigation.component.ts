import { Component, OnInit } from '@angular/core';
import {EstablishmentService} from '../../services/establishment.service';
import {EstablishmentEntity} from '../../model/establishment.entity';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  public establishments: EstablishmentEntity[] = [];

  constructor(private estService: EstablishmentService) {
    this.loadEstablishments();
  }

  ngOnInit() {
  }

  private loadEstablishments(): void {
    this.estService.listEstablishments().subscribe((ests: EstablishmentEntity[]) => {
      this.establishments = ests;
    }, (e) => {
      console.error('Error occurred when retrieving establishments from navigation bar', e);
    });
  }

}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {EstablishmentSubpageService} from '../../services/establishment-subpage.service';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {EstablishmentSubpageEntity} from '../../model/establishment-subpage.entity';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-establishment-subpage',
  templateUrl: './establishment-subpage.component.html',
  styleUrls: ['./establishment-subpage.component.scss']
})
export class EstablishmentSubpageComponent implements OnInit, OnDestroy {

  public subpage: EstablishmentSubpageEntity;
  private paramSubscription: Subscription;

  constructor(private subpageService: EstablishmentSubpageService, private title: Title, private router: Router, route: ActivatedRoute) {
    this.paramSubscription = route.paramMap.subscribe((obj) => {
      const estParam = +obj['params']['establishmentId'];
      const subpageRoute = obj['params']['subpageRoute'];
      console.log(estParam, subpageRoute);
      this.loadSubpage(estParam, subpageRoute);
    });
  }


  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.paramSubscription.unsubscribe();
  }

  public showGallery(): boolean {
    if (this.subpage.galleryFiles === undefined || this.subpage.galleryFiles === null) {
      return false;
    }

    return this.subpage.galleryFiles.length > 0;
  }

  private loadSubpage(establishmentId: number, subpageRoute: string): void {
    this.subpageService.getSubpage(establishmentId, subpageRoute).subscribe((subpage: EstablishmentSubpageEntity) => {
      this.subpage = subpage;
      this.title.setTitle('Studio Effect - ' + this.subpage.name);
    }, (e) => {
      this.router.navigate(['nenalezeno']);
    });
  }
}

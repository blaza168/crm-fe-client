import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstablishmentSubpageComponent } from './establishment-subpage.component';

describe('EstablishmentSubpageComponent', () => {
  let component: EstablishmentSubpageComponent;
  let fixture: ComponentFixture<EstablishmentSubpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstablishmentSubpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstablishmentSubpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

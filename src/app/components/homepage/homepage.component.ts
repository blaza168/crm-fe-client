import {Component, OnInit} from '@angular/core';
import {Hash, KvService} from '../../services/kv.service';
import {Title} from '@angular/platform-browser';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import {FileService} from '../../services/file.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  providers: [NgbCarouselConfig]
})
export class HomepageComponent implements OnInit {

  private static readonly CATEGORY = 'homepage';
  private static readonly FILE_CATEGORY = 'homepage';
  private static readonly FILE_KEY = 'default';
  public content: string; // HTML content

  public images: string[]; // urls of images

  constructor(
    private title: Title,
    private config: NgbCarouselConfig,
    private fileService: FileService,
    private kvService: KvService,
  ) {
    this.kvService.getHash(HomepageComponent.CATEGORY).subscribe((hash: Hash) => {
      this.content = hash.getValue('content');
    });
    this.fileService.listFiles(HomepageComponent.FILE_CATEGORY, HomepageComponent.FILE_KEY).subscribe((ids: number[]) => {
      if (ids.length !== 0) {
        this.images = ids.map((id) => `/api/crm/image/${id}`);
      }
    });

    config.interval = 5000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;
  }

  ngOnInit() {
    this.title.setTitle('Studio Effect - Salón Břeclav');
  }
}

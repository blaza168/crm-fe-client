import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {GalleryImage} from '../image-gallery/image-gallery.component';

@Component({
  selector: 'app-image-dialog',
  templateUrl: './image-dialog.component.html',
  styleUrls: ['./image-dialog.component.scss']
})
export class ImageDialogComponent implements OnInit {

  public currentIndex: number;
  public images: GalleryImage[];

  constructor(
    public dialogRef: MatDialogRef<ImageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {imgIndex: number, images: GalleryImage[]}
  ) {
    this.currentIndex = data.imgIndex;
    this.images = data.images;
  }

  ngOnInit() {
  }

  public idToImgSource(id: number): string {
    return `/api/crm/image/${id}`;
  }
}

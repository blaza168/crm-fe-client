import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ImageDialogComponent} from '../image-dialog/image-dialog.component';

export class GalleryImage {
  id: number; // id of file
  description?: string;
}

class RowImage extends GalleryImage {
  constructor(id: number, index: number, description?: string) {
    super();
    this.index = index;
    this.id = id;
    this.description = description;
  }
  index: number;
}

@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.scss']
})
export class ImageGalleryComponent implements OnInit, OnChanges {

  @Input() images: GalleryImage[];

  public imageRows: RowImage[][];

  constructor(private dialog: MatDialog) {}

  ngOnInit() {
    this.imageRows = this.imagesToRows(this.images);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const currentImages = changes.images.currentValue as GalleryImage[];
    this.imageRows = this.imagesToRows(currentImages);
  }

  private imagesToRows(images: GalleryImage[]): RowImage[][] {
    const rows = [];
    for (let i = 0; i < images.length; i++) {
      if (i % 3 === 0) {
        rows.push([]);
      }
      rows[rows.length - 1].push(
        new RowImage(images[i].id, i, images[i].description)
      );
    }

    return rows;
  }

  public onImgContainerClick(imageIndex: number): void {
    const dialogRef = this.dialog.open(ImageDialogComponent, {
      maxWidth: '70%',
      maxHeight: '80vh',
      data: {imgIndex: imageIndex, images: this.images}
    });
  }

  public idToImgSource(id: number): string {
    return `/api/crm/image/${id}`;
  }
}

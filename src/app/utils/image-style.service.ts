export class UrlService {

  constructor() {}

  public static getBaseUrl(): string {
    const prefix = window.location.protocol + '//';
    return prefix + window.location.hostname;
  }

}

export enum GradientStyle {
  TO_RIGTH = 'to right', TO_BOTTOM = 'to bottom'
}


export class Filter {
  gradient?: GradientStyle;
  firstColor: string; // RGBA
  secondColor?: string; // RGBA
}

export class Transform {
  attachment_fixed = false;
}

export class ImageStyle {
  filter = new Filter();
  transform = new Transform();
}

// TODO: rename this to imageStyleUtil
export class ImageStyleService {

  public static getBackgroundImage(style: ImageStyle, imageUrl: string): string {
    const gradient = ImageStyleService.getLinearGradient(style.filter);
    let url: string;
    if (imageUrl.length > 200) {
      // image is encoded
      url = 'url("' + imageUrl + '")';
    } else {
      url = 'url("' + UrlService.getBaseUrl() + imageUrl + '")';
    }

    return gradient + url;
  }

  public static getLinearGradient(filter: Filter): string {
    if (!filter.firstColor) {
      return ''; // EmptyFilter
    }
    let gradient = 'linear-gradient(';
    if (filter.gradient && filter.secondColor) {
      // two colors
      gradient += filter.gradient;
      gradient += ', ';
      gradient += filter.firstColor;
      gradient += ', ';
      gradient += filter.secondColor;
      gradient += ')'; // close linear gradient
      gradient += ',';
    } else {
      gradient += filter.firstColor;
      gradient += ',';
      gradient += filter.firstColor;
      gradient += ')';
      gradient += ',';
    }

    return gradient;
  }

}
